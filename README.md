# README #

setTimeoutLoop is a JavaScript function designed to allow convenient dynamic calling of a function a designated number of times, with a designated delay.

### Usage ###

Call setTimeoutLoop in this manner:


```
#!javascript

setTimeoutLoop(numberOfLoops, functionName[, delay]);
```

or
```
#!javascript

setTimeoutLoop(numberOfLoops, functionName, delay[, arg1, arg2 ...]);
```


For the function name, do not include the parentheses () after the name.

The first two variables (number of loops [integer] and the function name [without parentheses]) are required.

The delay is specified in milliseconds, and is optional.  If no delay is passed in, a default delay of 1000 milliseconds (1 second) will be applied.

To pass in arguments for the called function, include them individually after the delay.  (If passing in arguments, a delay must be specified; otherwise the first argument will be treated as the delay.)

For example, to run this code in a half-second-delayed loop, four times:
```
#!javascript

myFunct(var1, "Hi");
```

you would type:
```
#!javascript

setTimeoutLoop(4, myFunct, 500, var1, "Hi");
```

#### this ####

You can pass in the current `this` object when calling setTimeoutLoop. To pass `this` to the called function, simply append `.bind(this)` to the name of the function in the arguments list.  setTimeoutLoop will preserve the value and feed it into your function so you don't have to pass it in as an argument and accept it as a `that` or `self` parameter. 
 For example:

```
#!javascript

setTimeoutLoop(4, myFunct.bind(this), 500, var1, "Hi");
```


### Changelog ###

v1.1.1:

- fixed issue with passing setTimeoutLoop arguments to called function if a parameter was expected but no arguments were specified

v1.1:

- added support for passing in arguments

setTimeoutLoop is versioned using [Semantic Versioning](http://semver.org/).