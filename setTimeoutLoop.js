// setTimeoutLoop v1.1.1

function setTimeoutLoop(numLoops, theFunction, theDelay) {
    var counterVar = 0;
    var paramList;
    var obj = {};
    if (arguments.length < 2) {
        //if # loops and/or function not specified
        return;
    }
    else if (arguments.length < 3) {
        //if no delay specified
        theDelay = 1000;
    }
    else if (arguments.length > 3) {
    	//has parameters to pass in
    	paramList = Array.from(arguments).slice(3);
    }
    var loopID = setInterval(function(){
        counterVar++;
        theFunction.apply(obj,paramList);
        if (counterVar >= numLoops) {
            clearInterval(loopID);
        }
    }, theDelay);
}